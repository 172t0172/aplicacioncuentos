package com.example.cuentosparanios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_doce.*
import kotlinx.android.synthetic.main.activity_main.*

class DoceActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doce)

        button22.setOnClickListener{

            val intent: Intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        button23.setOnClickListener{

            val intent: Intent = Intent(this, TreceActivity::class.java)
            startActivity(intent)
        }
    }
}