package com.example.cuentosparanios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_trece.*

class TreceActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trece)

        button24.setOnClickListener{

            val intent: Intent = Intent(this, DoceActivity::class.java)
            startActivity(intent)
        }

        button25.setOnClickListener{

            val intent: Intent = Intent(this, CatorceActivity::class.java)
            startActivity(intent)
        }
    }
}