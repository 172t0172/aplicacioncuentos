package com.example.cuentosparanios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button;
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button6.setOnClickListener{

            val intent: Intent = Intent(this, SegundoActivity::class.java)
            startActivity(intent)
        }

        button4.setOnClickListener{
            val intent: Intent = Intent(this, TercerActivity::class.java)
            startActivity(intent)

        }

        button5.setOnClickListener{
            val intent: Intent = Intent(this, CuartoActivity::class.java)
        }


    }
}