package com.example.cuentosparanios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_cuarto.*
import kotlinx.android.synthetic.main.activity_main.*

class CuartoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuarto)

        button8.setOnClickListener{

            val intent: Intent = Intent(this, TercerActivity::class.java)
            startActivity(intent)
        }

        button9.setOnClickListener{

            val intent: Intent = Intent(this, QuintaActivity::class.java)
            startActivity(intent)
        }
    }
}