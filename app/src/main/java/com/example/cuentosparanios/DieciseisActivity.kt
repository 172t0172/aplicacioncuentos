package com.example.cuentosparanios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_dieciseis.*
import kotlinx.android.synthetic.main.activity_main.*

class DieciseisActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dieciseis)

        button30.setOnClickListener{

            val intent: Intent = Intent(this, QuinceActivity::class.java)
            startActivity(intent)
        }
    }
}