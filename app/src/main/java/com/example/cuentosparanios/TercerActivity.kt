package com.example.cuentosparanios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_tercer.*

class TercerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tercer)

        button3.setOnClickListener{

            val intent: Intent = Intent(this, SegundoActivity::class.java)
            startActivity(intent)
        }

        button7.setOnClickListener{

            val intent: Intent = Intent(this, CuartoActivity::class.java)
            startActivity(intent)
        }
    }
}