package com.example.cuentosparanios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_ocho.*

class OchoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ocho)

        button15.setOnClickListener{

            val intent: Intent = Intent(this, SieteActivity::class.java)
            startActivity(intent)
        }

        button16.setOnClickListener{

            val intent: Intent = Intent(this, NueveActivity::class.java)
            startActivity(intent)
        }
    }
}