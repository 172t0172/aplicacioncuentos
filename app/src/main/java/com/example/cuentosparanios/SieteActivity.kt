package com.example.cuentosparanios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_siete.*

class SieteActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_siete)

        button13.setOnClickListener{

            val intent: Intent = Intent(this, SextoActivity::class.java)
            startActivity(intent)
        }

        button14.setOnClickListener{

            val intent: Intent = Intent(this, OchoActivity::class.java)
            startActivity(intent)
        }

    }
}