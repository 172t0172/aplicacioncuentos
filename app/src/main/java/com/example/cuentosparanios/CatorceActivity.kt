package com.example.cuentosparanios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_catorce.*
import kotlinx.android.synthetic.main.activity_main.*

class CatorceActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catorce)

        button26.setOnClickListener{

            val intent: Intent = Intent(this, TreceActivity::class.java)
            startActivity(intent)
        }

        button27.setOnClickListener{

            val intent: Intent = Intent(this, QuinceActivity::class.java)
            startActivity(intent)
        }
    }
}