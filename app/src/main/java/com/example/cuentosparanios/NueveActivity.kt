package com.example.cuentosparanios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_nueve.*

class NueveActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nueve)

        button17.setOnClickListener{

            val intent: Intent = Intent(this, OchoActivity::class.java)
            startActivity(intent)
        }

        button18.setOnClickListener{

            val intent: Intent = Intent(this, DiezActivity::class.java)
            startActivity(intent)
        }
    }
}