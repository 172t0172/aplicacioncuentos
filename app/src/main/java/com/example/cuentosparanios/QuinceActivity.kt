package com.example.cuentosparanios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_quince.*

class QuinceActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quince)

        button28.setOnClickListener{

            val intent: Intent = Intent(this, CatorceActivity::class.java)
            startActivity(intent)
        }

        button29.setOnClickListener{

            val intent: Intent = Intent(this, DieciseisActivity::class.java)
            startActivity(intent)
        }
    }
}