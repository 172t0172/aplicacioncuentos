package com.example.cuentosparanios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_diez.*
import kotlinx.android.synthetic.main.activity_main.*

class DiezActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_diez)

        button19.setOnClickListener{

            val intent: Intent = Intent(this, NueveActivity::class.java)
            startActivity(intent)
        }

        button20.setOnClickListener{

            val intent: Intent = Intent(this, OnceActivity::class.java)
            startActivity(intent)
        }
    }
}